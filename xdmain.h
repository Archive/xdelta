/* -*- Mode: C;-*-
 *
 * This file is part of XDelta - A binary delta generator.
 *
 * Copyright (C) 1997, 1998  Josh MacDonald
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 *
 * $Id: xdmain.h 1.1 Sun, 11 Apr 1999 18:56:20 -0700 jmacd $
 */

#ifndef _XDMAIN_H_
#define _XDMAIN_H_

FileHandle* open_read_seek_handle   (const char* name, gboolean* is_compressed, gboolean cache);
FileHandle* open_read_noseek_handle (const char* name, gboolean* is_compressed);
FileHandle* open_write_handle       (int fd, const char* name, gboolean gzip);
FileHandle* compress_subhandle      (FileHandle* out, gint compress_level);
FileHandle* uncompress_subhandle    (FileHandle* in, guint beg, guint end, gboolean compress);

#endif
