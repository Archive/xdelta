/* -*- Mode: C;-*-
 *
 * This file is part of XDelta - A binary delta generator.
 *
 * Copyright (C) 1997, 1998, 1999  Josh MacDonald
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 *
 * $Id: xdhandle.c 1.2 Mon, 12 Apr 1999 00:56:11 -0700 jmacd $
 */

#include "xdelta.h"
#include "xdmain.h"

FileHandle* open_read_seek_handle   (const char* name, gboolean* is_compressed, gboolean cache) { return NULL; }
FileHandle* open_read_noseek_handle (const char* name, gboolean* is_compressed) { return NULL; }
FileHandle* open_write_handle       (int fd, const char* name, gboolean gzip) { return NULL; }
FileHandle* compress_subhandle      (FileHandle* out, gint compress_level) { return NULL; }
FileHandle* uncompress_subhandle    (FileHandle* in, guint beg, guint end, gboolean compress) { return NULL; }

#if 0
typedef struct _LRU LRU;

struct _LRU
{
  LRU *next;
  LRU *prev;

  gint refs;
  guint page;
  guint8* buffer;
};

typedef struct _XdFileHandle XdFileHandle;

struct _XdFileHandle
{
  FileHandle fh;

  guint    length;
  guint    real_length;
  gint     type;
  const char* name;
  const char* cleanup;

  guint8 md5[16];
  EdsioMD5Ctx ctx;

  /* for write */
  int out_fd;
  void* out;
  gboolean (* out_write) (XdFileHandle* handle, const void* buf, gint nbyte);
  gboolean (* out_close) (XdFileHandle* handle);

  /* for read */
  GPtrArray *lru_table;
  LRU       *lru_head;  /* most recently used. */
  LRU       *lru_tail;  /* least recently used. */
  GMemChunk *lru_chunk;
  guint      lru_count;
  guint      lru_outstanding_refs;

  guint    narrow_low;
  guint    narrow_high;
  guint    current_pos;
  FILE*    in;
  gboolean (* in_read) (XdFileHandle* handle, void* buf, gint nbyte);
  gboolean (* in_close) (XdFileHandle* handle);
  gboolean in_compressed;

  const guint8* copy_page;
  guint  copy_pgno;
  gboolean md5_good;
  gboolean reset_length_next_write;

  gint md5_page;
  gint fd;
};


#define READ_TYPE 1
#define READ_NOSEEK_TYPE 1
#define READ_SEEK_TYPE   3
#define WRITE_TYPE 4

/* Note to the casual reader: the filehandle implemented here is
 * highly aware of the calling patterns of the Xdelta library.  It
 * also plays games with narrowing to implement several files in the
 * same handle.  So, if you try to modify or use this code, BEWARE.
 * See the repository package for a complete implementation.
 * In fact, its really quite a hack. */

static gssize xd_handle_map_page (XdFileHandle *fh, guint pgno, const guint8** mem);
static gboolean xd_handle_unmap_page (XdFileHandle *fh, guint pgno, const guint8** mem);

static gboolean
xd_fwrite (XdFileHandle* fh, const void* buf, gint nbyte)
{
  return fwrite (buf, nbyte, 1, fh->out) == 1;
}

static gboolean
xd_fread (XdFileHandle* fh, void* buf, gint nbyte)
{
  return fread (buf, nbyte, 1, fh->in) == 1;
}

static gboolean
xd_fclose (XdFileHandle* fh)
{
  return fclose (fh->out) == 0;
}

static gboolean
xd_frclose (XdFileHandle* fh)
{
  return fclose (fh->in) == 0;
}

static gboolean
xd_gzwrite (XdFileHandle* fh, const void* buf, gint nbyte)
{
  return gzwrite (fh->out, (void*) buf, nbyte) == nbyte;
}

static gboolean
xd_gzread (XdFileHandle* fh, void* buf, gint nbyte)
{
  return gzread (fh->in, buf, nbyte) == nbyte;
}

static gboolean
xd_gzclose (XdFileHandle* fh)
{
  return gzclose (fh->out) == Z_OK;
}

static gboolean
xd_gzrclose (XdFileHandle* fh)
{
  return gzclose (fh->in) == Z_OK;
}

static void
init_table (XdFileHandle* fh)
{
  fh->lru_table = g_ptr_array_new ();
  fh->lru_chunk = g_mem_chunk_create(LRU, 1<<9, G_ALLOC_ONLY);
  fh->lru_head = NULL;
  fh->lru_tail = NULL;
}

static XdFileHandle*
open_common (const char* name, const char* real_name)
{
  XdFileHandle* fh;

  gint fd;
  struct stat buf;

  if ((fd = open (name, O_RDONLY | O_BINARY, 0)) < 0)
    {
      xd_error ("open %s failed: %s\n", name, g_strerror (errno));
      return NULL;
    }

  if (stat (name, &buf) < 0)
    {
      xd_error ("stat %s failed: %s\n", name, g_strerror (errno));
      return NULL;
    }

  /* S_ISREG() is not on Windows */
  if ((buf.st_mode & S_IFMT) != S_IFREG)
    {
      xd_error ("%s is not a regular file\n", name);
      return NULL;
    }

  fh = g_new0 (XdFileHandle, 1);

  fh->fh.table = & xd_handle_table;
  fh->name = real_name;
  fh->fd = fd;
  fh->length = buf.st_size;
  fh->narrow_high = buf.st_size;

  return fh;
}

static gboolean
file_gzipped (const char* name, gboolean *is_compressed)
{
  FILE* f = fopen (name, FOPEN_READ_ARG);
  guint8 buf[2];

  (*is_compressed) = FALSE;

  if (! f)
    {
      xd_error ("open %s failed: %s\n", name, g_strerror (errno));
      return FALSE;
    }

  if (fread (buf, 2, 1, f) != 1)
    return TRUE;

#define GZIP_MAGIC1 037
#define GZIP_MAGIC2 0213

  if (buf[0] == GZIP_MAGIC1 && buf[1] == GZIP_MAGIC2)
    (* is_compressed) = TRUE;

  return TRUE;
}

static const char*
xd_tmpname (void)
{
  const char* tmpdir = g_get_tmp_dir ();
  GString* s;
  gint x = getpid ();
  static gint seq = 0;
  struct stat buf;

  s = g_string_new (NULL);

  do
    {
      g_string_sprintf (s, "%s/xdtmp.%d.%d", tmpdir, x, seq++);
    }
  while (lstat (s->str, &buf) == 0);

  return s->str;
}

static const char*
file_gunzip (const char* name)
{
  const char* new_name = xd_tmpname ();
  FILE* out = fopen (new_name, FOPEN_WRITE_ARG);
  gzFile in = gzopen (name, "rb");
  guint8 buf[1024];
  int nread;

  while ((nread = gzread (in, buf, 1024)) > 0)
    {
      if (fwrite (buf, nread, 1, out) != 1)
	{
	  xd_error ("write %s failed (during uncompression): %s\n", new_name, g_strerror (errno));
	  return NULL;
	}
    }

  if (nread < 0)
    {
      xd_error ("gzread %s failed: %s\n", name, g_strerror (errno));
      return NULL;
    }

  gzclose (in);

  if (fclose (out))
    {
      xd_error ("close %s failed (during uncompression): %s\n", new_name, g_strerror (errno));
      return NULL;
    }

  return new_name;
}

static XdFileHandle*
open_read_noseek_handle (const char* name, gboolean* is_compressed, gboolean will_read, gboolean honor_pristine)
{
  XdFileHandle* fh;
  const char* name0 = name;

  /* we _could_ stream-read this file if compressed, but it adds a
   * lot of complexity.  the library can handle it, just set the
   * length to (XDELTA_MAX_FILE_LEN-1) and make sure that the end
   * of file condition is set when on the last page.  However, I
   * don't feel like it. */
  if (honor_pristine && pristine)
    *is_compressed = FALSE;
  else
    {
      if (! file_gzipped (name, is_compressed))
        return NULL;
    }

  if ((* is_compressed) && ! (name = file_gunzip (name)))
    return NULL;

  if (! (fh = open_common (name, name0)))
    return NULL;

  fh->type = READ_NOSEEK_TYPE;

  edsio_md5_init (&fh->ctx);

  if (*is_compressed)
    fh->cleanup = name;

  if (will_read)
    {
      g_assert (fh->fd >= 0);
      if (! (fh->in = fdopen (dup (fh->fd), FOPEN_READ_ARG)))
	{
	  xd_error ("fdopen: %s\n", g_strerror (errno));
	  return NULL;
	}
      fh->in_read = &xd_fread;
      fh->in_close = &xd_frclose;
    }
  else
    {
      init_table (fh);
    }

  return fh;
}

static void
xd_read_close (XdFileHandle* fh)
{
  if (fh->cleanup)
    unlink (fh->cleanup);

  close (fh->fd);

  if (fh->in)
    (*fh->in_close) (fh);
}

static XdFileHandle*
open_read_seek_handle (const char* name, gboolean* is_compressed, gboolean honor_pristine)
{
  XdFileHandle* fh;
  const char* name0 = name;

  if (honor_pristine && pristine)
    *is_compressed = FALSE;
  else
    {
      if (! file_gzipped (name, is_compressed))
	return NULL;
    }

  if ((* is_compressed) && ! (name = file_gunzip (name)))
    return NULL;

  if (! (fh = open_common (name, name0)))
    return NULL;

  fh->type = READ_SEEK_TYPE;

  if (*is_compressed)
    fh->cleanup = name;

  init_table (fh);

  edsio_md5_init (&fh->ctx);

  return fh;
}

static XdFileHandle*
open_write_handle (int fd, const char* name)
{
  XdFileHandle* fh = g_new0 (XdFileHandle, 1);
  int nfd;

  fh->fh.table = & xd_handle_table;
  fh->out_fd = fd;
  fh->out_write = &xd_fwrite;
  fh->out_close = &xd_fclose;

  g_assert (fh->out_fd >= 0);

  nfd = dup (fh->out_fd);

  if (! (fh->out = fdopen (nfd, FOPEN_WRITE_ARG)))
    {
      xd_error ("fdopen %s failed: %s\n", name, g_strerror (errno));
      return NULL;
    }

  fh->type = WRITE_TYPE;
  fh->name = name;

  edsio_md5_init (&fh->ctx);

  return fh;
}

static gint
xd_begin_compression (XdFileHandle* fh)
{
  gint filepos, nfd;

  if (compress_level == 0)
    return fh->real_length;

  if (! (fh->out_close) (fh))
    {
      xd_error ("fclose failed: %s\n", g_strerror (errno));
      return -1;
    }

  filepos = lseek (fh->out_fd, 0, SEEK_END);

  if (filepos < 0)
    {
      xd_error ("lseek failed: %s\n", g_strerror (errno));
      return -1;
    }

  g_assert (fh->out_fd >= 0);

  nfd = dup (fh->out_fd);

  fh->out = gzdopen (nfd, "wb");
  fh->out_write = &xd_gzwrite;
  fh->out_close = &xd_gzclose;

  if (! fh->out)
    {
      xd_error ("gzdopen failed: %s\n", g_strerror (errno));
      return -1;
    }

  if (gzsetparams(fh->out, compress_level, Z_DEFAULT_STRATEGY) != Z_OK)
    {
      int foo;
      xd_error ("gzsetparams failed: %s\n", gzerror (fh->out, &foo));
      return -1;
    }

  return filepos;
}

static gboolean
xd_end_compression (XdFileHandle* fh)
{
  if (compress_level == 0)
    return TRUE;

  if (! (fh->out_close) (fh))
    {
      xd_error ("fdclose failed: %s\n", g_strerror (errno));
      return FALSE;
    }

  if (lseek (fh->out_fd, 0, SEEK_END) < 0)
    {
      xd_error ("lseek failed: %s\n", g_strerror (errno));
      return FALSE;
    }

  g_assert (fh->out_fd >= 0);
  fh->out = fdopen (dup (fh->out_fd), FOPEN_WRITE_ARG);
  fh->out_write = &xd_fwrite;
  fh->out_close = &xd_fclose;

  if (! fh->out)
    {
      xd_error ("fdopen failed: %s\n", g_strerror (errno));
      return FALSE;
    }

  return TRUE;
}

static gssize
xd_handle_length (XdFileHandle *fh)
{
  if (fh->in_compressed)
    return fh->current_pos;
  else
    return fh->narrow_high - fh->narrow_low;
}

static gssize
xd_handle_pages (XdFileHandle *fh)
{
  g_assert (fh->type & READ_TYPE);
  return xd_handle_length (fh) / XD_PAGE_SIZE;
}

static gssize
xd_handle_pagesize (XdFileHandle *fh)
{
  g_assert (fh->type & READ_TYPE);
  return XD_PAGE_SIZE;
}

static gint
on_page (XdFileHandle* fh, guint pgno)
{
  if (pgno > xd_handle_pages (fh))
    return -1;

  if (pgno == xd_handle_pages (fh))
    return xd_handle_length (fh) % XD_PAGE_SIZE;

  return XD_PAGE_SIZE;
}

static gboolean
xd_handle_close (XdFileHandle *fh, gint ignore)
{
  /* this is really a reset for writable files */

  if (fh->type == WRITE_TYPE)
    {
      if (fh->reset_length_next_write)
	{
	  fh->reset_length_next_write = FALSE;
	  fh->length = 0;
	  fh->narrow_high = 0;
	}

      fh->reset_length_next_write = TRUE;
      edsio_md5_final (fh->md5, &fh->ctx);
      edsio_md5_init (&fh->ctx);
    }
  else if (fh->in)
    {
      edsio_md5_final (fh->md5, &fh->ctx);
      edsio_md5_init (&fh->ctx);
      fh->md5_good = FALSE;
    }

  return TRUE;
}

static const guint8*
xd_handle_checksum_md5 (XdFileHandle *fh)
{
  if (fh->in && ! fh->md5_good)
    {
      edsio_md5_final (fh->md5, &fh->ctx);
      fh->md5_good = TRUE;
    }
  else if (fh->type != WRITE_TYPE && !fh->in)
    {
      const guint8* page;

      while (fh->md5_page <= xd_handle_pages (fh))
	{
	  gint pgno = fh->md5_page;
	  gint onpage;

	  if ((onpage = xd_handle_map_page (fh, pgno, &page)) < 0)
	    return NULL;

	  if (pgno == fh->md5_page)
	    {
	      fh->md5_page += 1;
	      edsio_md5_update (&fh->ctx, page, onpage);

	      if (fh->md5_page > xd_handle_pages (fh))
		edsio_md5_final (fh->md5, &fh->ctx);
	    }

	  if (! xd_handle_unmap_page (fh, pgno, &page))
	    return NULL;
	}
    }

  return g_memdup (fh->md5, 16);
}

static gboolean
xd_handle_set_pos (XdFileHandle *fh, guint pos)
{
  if (fh->current_pos == pos + fh->narrow_low)
    return TRUE;

  if (pos + fh->narrow_low > fh->narrow_high)
    {
      xd_error ("unexpected EOF in %s\n", fh->name);
      return FALSE;
    }

  fh->current_pos = pos + fh->narrow_low;

  if (fseek (fh->in, fh->current_pos, SEEK_SET))
    {
      xd_error ("fseek failed: %s\n", g_strerror (errno));
      return FALSE;
    }

  return TRUE;
}

static gboolean
xd_handle_narrow (XdFileHandle* fh, guint low, guint high, gboolean compressed)
{
  if (high > fh->length)
    {
      xd_error ("%s: corrupt or truncated delta\n", fh->name);
      return FALSE;
    }

  fh->narrow_low = low;
  fh->narrow_high = high;

  edsio_md5_init (&fh->ctx);

  if (compressed)
    {
      (* fh->in_close) (fh);

      if (lseek (fh->fd, low, SEEK_SET) < 0)
	{
	  xd_error ("%s: corrupt or truncated delta: cannot seek to %d: %s\n", fh->name, low, g_strerror (errno));
	  return FALSE;
	}

      g_assert (fh->fd >= 0);
      fh->in = gzdopen (dup (fh->fd), "rb");
      fh->in_read =  &xd_gzread;
      fh->in_close = &xd_gzrclose;
      fh->in_compressed = TRUE;
      fh->current_pos = 0;

      if (! fh->in)
	{
	  xd_error ("gzdopen failed: %s\n", g_strerror (errno));
	  return -1;
	}
    }
  else
    {
      if (! xd_handle_set_pos (fh, 0))
	return FALSE;
    }

  return TRUE;
}

static guint
xd_handle_get_pos (XdFileHandle* fh)
{
  return fh->current_pos - fh->narrow_low;
}

static const gchar*
xd_handle_name (XdFileHandle *fh)
{
  return g_strdup (fh->name);
}

static gssize
xd_handle_read (XdFileHandle *fh, guint8 *buf, gsize nbyte)
{
  if (nbyte == 0)
    return 0;

  if (! (fh->in_read) (fh, buf, nbyte)) /* This is suspicious */
    {
      xd_error ("read failed: %s\n", g_strerror (errno));
      return -1;
    }

  if (!no_verify)
    edsio_md5_update (&fh->ctx, buf, nbyte);

  fh->current_pos += nbyte;

  return nbyte;
}

static gboolean
xd_handle_write (XdFileHandle *fh, const guint8 *buf, gsize nbyte)
{
  g_assert (fh->type == WRITE_TYPE);

  if (fh->reset_length_next_write)
    {
      fh->reset_length_next_write = FALSE;
      fh->length = 0;
      fh->narrow_high = 0;
    }

  if (! no_verify)
    edsio_md5_update (&fh->ctx, buf, nbyte);

  if (! (*fh->out_write) (fh, buf, nbyte))
    {
      xd_error ("write failed: %s\n", g_strerror (errno));
      return FALSE;
    }

  fh->length += nbyte;
  fh->real_length += nbyte;
  fh->narrow_high += nbyte;

  return TRUE;
}

static gboolean
xd_handle_really_close (XdFileHandle *fh)
{
  g_assert (fh->type == WRITE_TYPE);

  if (! (* fh->out_close) (fh) || close (fh->out_fd) < 0)
    {
      xd_error ("write failed: %s\n", g_strerror (errno));
      return FALSE;
    }

  return TRUE;
}

static LRU*
pull_lru (XdFileHandle* fh, LRU* lru)
{
  if (lru->next && lru->prev)
    {
      lru->next->prev = lru->prev;
      lru->prev->next = lru->next;
    }
  else if (lru->next)
    {
      fh->lru_tail = lru->next;
      lru->next->prev = NULL;
    }
  else if (lru->prev)
    {
      fh->lru_head = lru->prev;
      lru->prev->next = NULL;
    }
  else
    {
      fh->lru_head = NULL;
      fh->lru_tail = NULL;
    }

  lru->next = NULL;
  lru->prev = NULL;

  return lru;
}

static gboolean
really_free_one_page (XdFileHandle* fh)
{
  LRU *lru = fh->lru_tail;

  for (; lru; lru = lru->prev)
    {
      gint to_unmap;
      LRU *lru_dead;

      if (lru->refs > 0)
	continue;

      lru_dead = pull_lru (fh, lru);

      g_assert (lru_dead->buffer);

      to_unmap = on_page (fh, lru_dead->page);

      fh->lru_count -= 1;

      if (to_unmap > 0)
	{
#ifdef WIN32
	  g_free (lru_dead->buffer);
#else
	  if (munmap (lru_dead->buffer, to_unmap))
	    {
	      xd_error ("munmap failed: %s\n", g_strerror (errno));
	      return FALSE;
	    }
#endif
	}

      lru_dead->buffer = NULL;

      return TRUE;
    }

  return TRUE;
}

#if 0
static void
print_lru (XdFileHandle* fh)
{
  LRU* lru = fh->lru_head;

  for (; lru; lru = lru->prev)
    {
      g_print ("page %d buffer %p\n", lru->page, lru->buffer);

      if (! lru->prev && lru != fh->lru_tail)
	g_print ("incorrect lru_tail\n");
    }
}
#endif

static gboolean
make_lru_room (XdFileHandle* fh)
{
  if (fh->lru_count == max_mapped_pages)
    {
      if (! really_free_one_page (fh))
	return FALSE;
    }

  g_assert (fh->lru_count < max_mapped_pages);

  return TRUE;
}

/*#define DEBUG_MAP*/

static gssize
xd_handle_map_page (XdFileHandle *fh, guint pgno, const guint8** mem)
{
  LRU* lru;
  guint to_map;

#ifdef DEBUG_MAP
  g_print ("map %p:%d\n", fh, pgno);
#endif

  g_assert (fh->type & READ_TYPE);

  if (fh->lru_table->len < (pgno + 1))
    {
      gint olen = fh->lru_table->len;

      g_ptr_array_set_size (fh->lru_table, pgno + 1);

      while (olen <= pgno)
	fh->lru_table->pdata[olen++] = NULL;
    }

  lru = fh->lru_table->pdata[pgno];

  if (! lru)
    {
      lru = g_chunk_new0 (LRU, fh->lru_chunk);
      fh->lru_table->pdata[pgno] = lru;
      lru->page = pgno;
    }
  else if (lru->buffer)
    {
      pull_lru (fh, lru);
    }

  lru->prev = fh->lru_head;
  lru->next = NULL;

  fh->lru_head = lru;

  if (lru->prev)
    lru->prev->next = lru;

  if (! fh->lru_tail)
    fh->lru_tail = lru;

  to_map = on_page (fh, pgno);

  if (to_map < 0)
    {
      xd_error ("unexpected EOF in %s\n", fh->name);
      return -1;
    }

  if (! lru->buffer)
    {
      if (! make_lru_room (fh))
	return -1;

      fh->lru_count += 1;

      if (to_map > 0)
	{
#ifdef WIN32
	  lru->buffer = g_malloc (to_map);

	  if (lseek (fh->fd, pgno * XD_PAGE_SIZE, SEEK_SET) < 0)
	    {
	      xd_error ("lseek failed: %s\n", g_strerror (errno));
	      return -1;
	    }

	  if (read (fh->fd, lru->buffer, to_map) != to_map)
	    {
	      xd_error ("read failed: %s\n", g_strerror (errno));
	      return -1;
	    }
#else
	  if (! (lru->buffer = mmap (NULL, to_map, PROT_READ, MAP_PRIVATE, fh->fd, pgno * XD_PAGE_SIZE)))
	    {
	      xd_error ("mmap failed: %s\n", g_strerror (errno));
	      return -1;
	    }
#endif
	}
      else
	{
	  lru->buffer = (void*) -1;
	}

      if (pgno == fh->md5_page)
	{
	  if (! no_verify)
	    edsio_md5_update (&fh->ctx, lru->buffer, to_map);
	  fh->md5_page += 1;

	  if (fh->md5_page > xd_handle_pages (fh))
	    edsio_md5_final (fh->md5, &fh->ctx);
	}
    }

  (*mem) = lru->buffer;

  lru->refs += 1;
  fh->lru_outstanding_refs += 1;

  return to_map;
}

static gboolean
xd_handle_unmap_page (XdFileHandle *fh, guint pgno, const guint8** mem)
{
  LRU* lru;

#ifdef DEBUG_MAP
  g_print ("unmap %p:%d\n", fh, pgno);
#endif

  g_assert (fh->type & READ_TYPE);

  g_assert (pgno < fh->lru_table->len);

  lru = fh->lru_table->pdata[pgno];

  g_assert (lru && lru->refs > 0);

  g_assert (lru->buffer == (*mem));

  (*mem) = NULL;

  lru->refs -= 1;
  fh->lru_outstanding_refs += 1;

  if (lru->refs == 0 && fh->type == READ_NOSEEK_TYPE)
    {
      pull_lru (fh, lru);

      lru->next = fh->lru_tail;
      if (lru->next) lru->next->prev = lru;
      lru->prev = NULL;
      fh->lru_tail = lru;

      if (! really_free_one_page (fh))
	return FALSE;
    }

  return TRUE;
}

static gboolean
xd_handle_copy (XdFileHandle *from, XdFileHandle *to, guint off, guint len)
{
  if (from->in)
    {
      guint8 buf[1024];

      /*if (! xd_handle_set_pos (from, off))
	return FALSE;*/

      while (len > 0)
	{
	  guint r = MIN (1024, len);

	  if (xd_handle_read (from, buf, r) != r)
	    return FALSE;

	  if (! xd_handle_write (to, buf, r))
	    return FALSE;

	  len -= r;
	}
    }
  else
    {
      while (len > 0)
	{
	  guint off_page = off / XD_PAGE_SIZE;
	  guint off_off = off % XD_PAGE_SIZE;

	  gint on = on_page (from, off_page);
	  guint rem;
	  guint copy;

	  if (on <= 0)
	    {
	      xd_error ("unexpected EOF in %s\n", from->name);
	      return FALSE;
	    }

	  rem = on - off_off;
	  copy = MIN (len, rem);

	  if (from->copy_pgno != off_page &&
	      from->copy_page &&
	      ! xd_handle_unmap_page (from, from->copy_pgno, &from->copy_page))
	    return FALSE;

	  from->copy_pgno = off_page;

	  if (xd_handle_map_page (from, off_page, &from->copy_page) < 0)
	    return FALSE;

	  if (! xd_handle_write (to, from->copy_page + off_off, copy))
	    return FALSE;

	  if (! xd_handle_unmap_page (from, off_page, &from->copy_page))
	    return FALSE;

	  len -= copy;
	  off += copy;
	}
    }

  return TRUE;
}

static gboolean
xd_handle_putui (XdFileHandle *fh, guint32 i)
{
  guint32 hi = g_htonl (i);

  return xd_handle_write (fh, (guint8*)&hi, 4);
}

static gboolean
xd_handle_getui (XdFileHandle *fh, guint32* i)
{
  if (xd_handle_read (fh, (guint8*)i, 4) != 4)
    return FALSE;

  *i = g_ntohl (*i);

  return TRUE;
}

#if 0
static HandleFuncTable xd_handle_table =
{
  (gssize (*) (FileHandle *fh)) xd_handle_length,
  (gssize (*) (FileHandle *fh)) xd_handle_pages,
  (gssize (*) (FileHandle *fh)) xd_handle_pagesize,
  (gssize (*) (FileHandle *fh, guint pgno, const guint8** mem)) xd_handle_map_page,
  (gboolean (*) (FileHandle *fh, guint pgno, const guint8** mem)) xd_handle_unmap_page,
  (const guint8* (*) (FileHandle *fh)) xd_handle_checksum_md5,

  (gboolean (*) (FileHandle *fh, gint flags)) xd_handle_close,

  (gboolean (*) (FileHandle *fh, const guint8 *buf, gsize nbyte)) xd_handle_write,
  (gboolean (*) (FileHandle *from, FileHandle *to, guint off, guint len)) xd_handle_copy,

  (gboolean (*) (FileHandle *fh, guint32* i)) xd_handle_getui,
  (gboolean (*) (FileHandle *fh, guint32 i)) xd_handle_putui,
  (gssize   (*) (FileHandle *fh, guint8 *buf, gsize nbyte)) xd_handle_read,
  (const gchar* (*) (FileHandle *fh)) xd_handle_name,
};
#endif
#endif
