/* -*- Mode: C;-*-
 *
 * This file is part of XDelta - A binary delta generator.
 *
 * Copyright (C) 1997, 1998  Josh MacDonald
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 *
 * $Id: xdeltatest.c 1.2 Fri, 09 Apr 1999 15:24:35 -0700 jmacd $
 */

#include <sys/types.h>
#include <unistd.h>
#include "config.h"

#if TIME_WITH_SYS_TIME
# include <sys/time.h>
# include <time.h>
#else
# if HAVE_SYS_TIME_H
#  include <sys/time.h>
# else
#  include <time.h>
# endif
#endif

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/resource.h>
#include <stdio.h>

#include "xdelta.h"
#include <repo.h>


#define FILL_SIZE (1<<16)

guint8 fill_buffer[FILL_SIZE];

void
fail ()
{
  g_warning ("FAILURE\n");

  exit (1);
}

File*
empty_file ()
{
  static gint count = 0;
  char buf[10];

  sprintf (buf, "file%d", count++);

  return file_initialize (_fs_repo, path_canonicalize (_fs_repo, buf));
}

File*
fill_file (guint len, gint flags, guint8* md5)
{
  File* f = empty_file ();
  FileHandle* fh;
  gint i;
  EdsioMD5Ctx ctx;

  g_assert (len < FILL_SIZE);

  for (i = 0; i < len; i += 1)
    {
      if (i & (1<<9))
	fill_buffer[i] = i&0xff;
      else
	fill_buffer[i] = 255 - (i&0xff);
    }

  edsio_md5_init (&ctx);
  edsio_md5_update (&ctx, fill_buffer, len);
  edsio_md5_final (md5, &ctx);

  fh = file_open (f, HV_Replace | HV_NoSeek | flags);

  if (! handle_write (fh, fill_buffer, len))
    fail ();

  if (! handle_close (fh, CF_None))
    fail ();

  return f;
}

void
compare_files (File* fromfile, File* tofile, gint fromflags, gint toflags)
{
  gint pos = 0;

  FileHandle* toh;
  FileHandle* fromh;

  guint8 buf1[1024], buf2[1024];

  toh = file_open (tofile, HV_Read | HV_NoSeek | toflags);
  fromh = file_open (fromfile, HV_Read | HV_NoSeek | fromflags);

  if (!toh || !fromh)
    fail ();

  for (;;)
    {
      gint readf = handle_read (fromh, buf1, 1024);
      gint readt = handle_read (toh, buf2, 1024);
      gint i, m = MIN(readf, readt);

      if (readf < 0 || readt < 0)
	fail ();

      for (i = 0; i < m; i += 1, pos += 1)
	{
	  if (buf1[i] != buf2[i])
	    fail ();
	}

      if (m != 1024)
	{
	  if (readt == readf)
	    {
	      handle_close (toh, CF_None);
	      handle_close (fromh, CF_None);
	      return;
	    }

	  fail ();
	}
    }
}

const char* cmd_delta_program = "../xdelta";
const char* cmd_delta_profile = "xdelta";
const char* cmd_data_source = "data";
guint16     cmd_seed[3] = { 47384, 8594, 27489 };
guint       cmd_size = 1<<15;
guint       cmd_reps = 100;
guint       cmd_changes = 16;
guint       cmd_deletion_length = 128;
guint       cmd_insertion_length = 256;

FileHandle* data_source_handle;
guint data_source_length;

File* data_source_file;
Path* data_source_path;

typedef struct _Instruction Instruction;

struct _Instruction {
  guint32 offset;
  guint32 length;
  Instruction* next;
};

gboolean
write_file (File* file, Instruction* inst)
{
  FileHandle* h;

  if (! (h = file_open (file, HV_Replace)))
    return FALSE;

  for (; inst; inst = inst->next)
    {
      if (! handle_copy (data_source_handle, h, inst->offset, inst->length))
	return FALSE;
    }

  if (! handle_close (h, CF_None))
    return FALSE;

  return TRUE;
}

static struct timeval utime;
static struct timeval stime;
static long dsize;

void add_tv (struct timeval* accum, struct timeval* val)
{
  accum->tv_sec += val->tv_sec;
  accum->tv_usec += val->tv_usec;

  if (accum->tv_usec >= 1000000)
    {
      accum->tv_usec %= 1000000;
      accum->tv_sec += 1;
    }
}

gboolean
run_command (File* from, File* to, File* out, gboolean accounting)
{
  int pid, status, outfd;
  struct rusage usage;
  struct stat sbuf;

  const char* from_file = g_strdup (path_to_string (_fs_repo, file_access_path (from)));
  const char* to_file   = g_strdup (path_to_string (_fs_repo, file_access_path (to)));
  const char* out_file  = g_strdup (path_to_string (_fs_repo, file_access_path (out)));

  unlink (out_file);

  if ((pid = vfork()) < 0)
    return FALSE;

  if (pid == 0)
    {
      outfd = open (out_file, O_CREAT | O_TRUNC | O_WRONLY, 0777);

      if (outfd < 0)
	{
	  perror ("open");
	  fail ();
	}

      dup2(outfd, STDOUT_FILENO);

      if (close (outfd))
	{
	  perror ("close");
	  fail ();
	}

      if (strcmp (cmd_delta_profile, "xdelta") == 0)
	execl (cmd_delta_program,
	       cmd_delta_program,
	       "delta",
	       "-qn0",
	       from_file,
	       to_file,
	       NULL);
      else if (strcmp (cmd_delta_profile, "diff") == 0)
	execl (cmd_delta_program,
	       cmd_delta_program,
	       "--rcs",
	       "-a",
	       from_file,
	       to_file,
	       NULL);
      else
	{
	  g_warning ("delta profile did not match, must be one of: xdelta, diff\n");
	  _exit (127);
	}

      perror ("execl failed");
      _exit (127);
    }

  if (wait4 (pid, &status, 0, & usage) != pid)
    {
      perror ("wait failed");
      fail ();
    }

  if (! WIFEXITED (status) || WEXITSTATUS (status) > 1)
    {
      g_warning ("delta command failed\n");
      fail ();
    }

  if (stat (out_file, & sbuf))
    {
      perror ("stat");
      fail ();
    }

  if (accounting)
    {
      add_tv (& utime, & usage.ru_utime);
      add_tv (& stime, & usage.ru_stime);
      dsize += sbuf.st_size;
    }

  return TRUE;
}

void
report (void)
{
  double t = (utime.tv_sec + stime.tv_sec + (utime.tv_usec + stime.tv_usec) / 1000000.0) / (double) cmd_reps;
  double s = (dsize / (double) cmd_reps) / (double) (cmd_changes * cmd_insertion_length);

  g_print ("time %f comp %f\n", t, s);
}

guint32
random_offset (guint len)
{
  return lrand48 () % (data_source_length - len);
}

Instruction*
perform_change_rec (Instruction* inst, guint32 change_off, guint* total_len)
{
  if (change_off < inst->length)
    {
      guint32 to_delete = cmd_deletion_length;
      guint32 avail = inst->length;
      guint32 this_delete = MIN (to_delete, avail);
      Instruction* new_inst;

      inst->length -= this_delete;
      to_delete -= this_delete;

      while (to_delete > 0 && inst->next->length < to_delete)
	{
	  to_delete -= inst->next->length;
	  inst->next = inst->next->next;
	}

      if (to_delete > 0)
	inst->next->offset += to_delete;

      new_inst = g_new0 (Instruction, 1);

      new_inst->offset = random_offset (cmd_insertion_length);
      new_inst->length = cmd_insertion_length;
      new_inst->next = inst->next;
      inst->next = new_inst;

      (* total_len) += cmd_insertion_length - cmd_deletion_length;

      return inst;
    }
  else
    {
      inst->next = perform_change_rec (inst->next, change_off - inst->length, total_len);
      return inst;
    }
}

Instruction*
perform_change (Instruction* inst, guint* len)
{
  return perform_change_rec (inst, lrand48() % ((* len) - cmd_deletion_length), len);
}

void
test1 ()
{
  File* out_file;
  File* from_file;
  File* to_file;

  guint i, change, current_size = cmd_size;
  guint end_size = (cmd_changes * cmd_insertion_length) + cmd_size;
  Instruction* inst;

  seed48 (cmd_seed);

  if (! (data_source_path = path_canonicalize (_fs_repo, cmd_data_source)))
    fail ();

  if (! (data_source_file = file_initialize (_fs_repo, data_source_path)))
    fail ();

  if (file_is_not_type (data_source_file, FV_RegularFile))
    fail ();

  if (! (data_source_handle = file_open (data_source_file, HV_Read)))
    fail ();

  data_source_length = file_length (data_source_file);

  /* arbitrary checks */
  if (data_source_length < (64 * end_size))
    g_warning ("data source should be longer\n");

  if ((cmd_changes * cmd_deletion_length) > cmd_size)
    {
      g_warning ("no copies are expected\n");
      fail ();
    }

  inst = g_new0 (Instruction, 1);

  inst->offset = random_offset (cmd_size);
  inst->length = cmd_size;

  from_file = empty_file ();

  if (! write_file (from_file, inst))
    fail ();

  for (change = 0; change < cmd_changes; change += 1)
    inst = perform_change (inst, & current_size);

  to_file = empty_file ();

  if (! write_file (to_file, inst))
    fail ();

  if (! repository_commit (_fs_repo))
    fail ();

  out_file = empty_file ();

  /* warm caches */
  if (! run_command (from_file, to_file, out_file, FALSE) ||
      ! run_command (from_file, to_file, out_file, FALSE))
    fail ();

  for (i = 0; i < cmd_reps; i += 1)
    {
      if (! run_command (from_file, to_file, out_file, TRUE))
	fail ();
    }

  report ();
}

int
main (gint argc, gchar** argv)
{
  if (argc != 9)
    {
      g_print ("usage: %s PROGRAM PROFILE DATASOURCE FILESIZE REPETITIONS CHANGES INSERTIONLENGTH DELETIONLENGTH\n", argv[0]);
      exit (2);
    }

  cmd_delta_program = argv[1];
  cmd_delta_profile = argv[2];
  cmd_data_source = argv[3];

  if (! strtoui_checked (argv[4], & cmd_size, "Size"))
    return 2;

  if (! strtoui_checked (argv[5], & cmd_reps, "Repetitions"))
    return 2;

  if (! strtoui_checked (argv[6], & cmd_changes, "Changes"))
    return 2;

  if (! strtoui_checked (argv[7], & cmd_insertion_length, "Insertion length"))
    return 2;

  if (! strtoui_checked (argv[8], & cmd_deletion_length, "Deletion length"))
    return 2;

  /* body */

  system ("rm -rf /tmp/xdeltatest");

  repository_system_init ();

  test1 ();

  if (! repository_system_close ())
    fail ();

  system ("rm -rf /tmp/xdeltatest");

  return 0;
}
