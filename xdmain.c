/* -*- Mode: C;-*-
 *
 * This file is part of XDelta - A binary delta generator.
 *
 * Copyright (C) 1997, 1998, 1999  Josh MacDonald
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 *
 * $Id: xdmain.c 1.24 Fri, 17 Dec 1999 14:01:40 -0800 jmacd $
 */

/*#include <stdio.h>*/
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>

#include <fcntl.h>
#include <sys/stat.h>

#ifndef _WIN32
#include <unistd.h>
#include <sys/mman.h>
#define O_BINARY 0
#else
#include <io.h>
#include <process.h>
#define STDOUT_FILENO 1
#define lstat stat
#define S_IFMT _S_IFMT
#define S_IFREG _S_IFREG
#endif

#include <zlib.h>

#include "xdelta.h"
#include "xdmain.h"
#include "edsiostdio.h"

extern HandleFuncTable xd_handle_table;

#define XD_PAGE_SIZE (1<<20)

#define XDELTA_110_PREFIX "%XDZ004%"
#define XDELTA_104_PREFIX "%XDZ003%"
#define XDELTA_100_PREFIX "%XDZ002%"
#define XDELTA_020_PREFIX "%XDZ001%"
#define XDELTA_018_PREFIX "%XDZ000%"
#define XDELTA_014_PREFIX "%XDELTA%"
#define XDELTA_PREFIX     XDELTA_110_PREFIX
#define XDELTA_PREFIX_LEN 8

#define HEADER_WORDS (6)
#define HEADER_SPACE (HEADER_WORDS*4)
/* The header is composed of 4-byte words (network byte order) as follows:
 * word 1: flags
 * word 2: (from name length) << 16 | (to name length)
 * word 3: (reserved)
 * word 4: (reserved)
 * word 5: (reserved)
 * word 6: (reserved)
 * flags are:
 */
#define FLAG_NO_VERIFY 1
#define FLAG_FROM_COMPRESSED 2
#define FLAG_TO_COMPRESSED 4
#define FLAG_PATCH_COMPRESSED 8
/* and, the header is follwed by the from file name, then the to file
 * name, then the data. */

#ifdef _WIN32
#define FOPEN_READ_ARG "rb"
#define FOPEN_WRITE_ARG "wb"
#define FILE_SEPARATOR '\\'
#else
#define FOPEN_READ_ARG "r"
#define FOPEN_WRITE_ARG "w"
#define FILE_SEPARATOR '/'
#endif

#include "getopt.h"

typedef struct {
  gboolean       patch_is_compressed;
  const gchar*   patch_name;
  guint          patch_flags;
  const gchar*   patch_version;
  gboolean       has_trailer;

  XdeltaSourceInfo* data_source;
  XdeltaSourceInfo* from_source;

  gchar         *from_name;
  gchar         *to_name;

  guint          control_offset;
  guint          header_offset;

  gint16         from_name_len;
  gint16         to_name_len;

  guint32        header_space[HEADER_WORDS];
  guint8         magic_buf[XDELTA_PREFIX_LEN];

  FileHandle      *patch_in;
  FileHandle      *patch_data;

  XdeltaControl   *cont;
} XdeltaPatch;

/* $Format: "static const char xdelta_version[] = \"$ReleaseVersion$\"; " $ */
static const char xdelta_version[] = "2.0.0";

typedef struct _Command Command;

struct _Command {
  gchar* name;
  gint (* func) (gint argc, gchar** argv);
  gint nargs;
};

static gint    delta_command    (gint argc, gchar** argv);
static gint    patch_command    (gint argc, gchar** argv);
static gint    info_command     (gint argc, gchar** argv);

static const Command commands[] =
{
  { "delta",    delta_command,    -1 },
  { "patch",    patch_command,    -1 },
  { "info",     info_command,     1 },
  { NULL, NULL, 0 }
};

static struct option const long_options[] =
{
  {"help",                no_argument, 0, 'h'},
  {"version",             no_argument, 0, 'v'},
  {"verbose",             no_argument, 0, 'V'},
  {"noverify",            no_argument, 0, 'n'},
  {"pristine",            no_argument, 0, 'p'},
  {"quiet",               no_argument, 0, 'q'},
  /*{"long-long-format",    no_argument, 0, 'L'},
    {"long-format",         no_argument, 0, 'l'},*/
  {"maxmem",              required_argument, 0, 'm'},
  {0,0,0,0}
};

static const gchar* program_name;
static gint         compress_level = Z_DEFAULT_COMPRESSION;
static gint         no_verify = FALSE;
static gint         pristine = FALSE;
static gint         verbose = FALSE;
static gint         max_mapped_pages = G_MAXINT;
static gint         quiet = FALSE;
/*static gint         long_format = FALSE;
static gint         really_long_format = FALSE;*/

#define xd_error g_warning

static void
usage ()
{
  xd_error ("usage: %s COMMAND [OPTIONS] [ARG1 ...]\n", program_name);
  xd_error ("use --help for more help\n");
  exit (2);
}

static void
help ()
{
  xd_error ("usage: %s COMMAND [OPTIONS] [ARG1 ARG2 ...]\n", program_name);
  xd_error ("COMMAND is one of:\n");
  xd_error ("  delta     Produce a delta from ARG1 to ARG2 producing ARG3\n");
  xd_error ("  info      List details about delta ARG1\n");
  xd_error ("  patch     Patch file ARG2 with ARG1 producing ARG3\n");
  xd_error ("OPTIONS are:\n");
  xd_error ("  -v, --version\n");
  xd_error ("  -V, --verbose\n");
  xd_error ("  -h, --help\n");
  xd_error ("  -n, --noverify\n");
  xd_error ("  -p, --pristine\n");
  xd_error ("  -m, --maxmem=SIZE  Set the buffer size limit, e.g. 640K, 16M\n");
  xd_error ("  -[0-9]    Compression level: 0=none, 1=fast, 6=default, 9=best\n");
  exit (2);
}

static void
version ()
{
  xd_error ("version %s\n", xdelta_version);
  exit (2);
}

static FileHandle* xd_error_handle = NULL;

static void
xd_error_func (const gchar   *log_domain,
	       GLogLevelFlags	log_level,
	       const gchar   *message,
	       gpointer	user_data)
{
  if (! xd_error_handle)
    xd_error_handle = _stderr_handle;

  handle_printf (xd_error_handle, "%s: %s", program_name, message);
}

static gboolean
event_devel (void)
{
  static gboolean once = FALSE;
  static gboolean devel = FALSE;

  if (! once)
    {
      devel = g_getenv ("EDSIO_DEVEL") != NULL;
      once = TRUE;
    }

  return devel;
}

static gboolean
event_watch (GenericEvent* ev, GenericEventDef* def, const char* message)
{
  if (quiet && def->level <= EL_Warning)
    return TRUE;

  if (event_devel ())
    fprintf (stderr, "%s:%d: %s\n", ev->srcfile, ev->srcline, message);
  else
    fprintf (stderr, "%s: %s\n", program_name, message);

  return TRUE;
}

gint
main (gint argc, gchar** argv)
{
  const Command *cmd = NULL;
  gint c;
  gint longind;

  if (! edsio_library_init ())
    return 2;

  eventdelivery_event_watch_all (event_watch);

  if (! xd_edsio_init ())
    return 2;

  program_name = g_basename (argv[0]);

  g_log_set_handler (G_LOG_DOMAIN,
		     G_LOG_LEVEL_WARNING,
		     xd_error_func,
		     NULL);

  if (argc < 2)
    usage ();

  for (cmd = commands; cmd->name; cmd += 1)
    if (strcmp (cmd->name, argv[1]) == 0)
      break;

  if (strcmp (argv[1], "-h") == 0 ||
      strcmp (argv[1], "--help") == 0)
    help ();

  if (strcmp (argv[1], "-v") == 0 ||
      strcmp (argv[1], "--version") == 0)
    version ();

  if (!cmd->name)
    {
      xd_error ("unrecognized command\n");
      help ();
    }

  argc -= 1;
  argv += 1;

  while ((c = getopt_long(argc,
			  argv,
			  "+nqphvVm:0123456789",
			  long_options,
			  &longind)) != EOF)
    {
      switch (c)
	{
	  /*case 'l': long_format = TRUE; break;
	    case 'L': really_long_format = TRUE; break;*/
	case 'q': quiet = TRUE; break;
	case 'n': no_verify = TRUE; break;
	case 'p': pristine = TRUE; break;
	case 'V': verbose = TRUE; break;
	case 'm':
	  {
	    gchar* end = NULL;
	    glong l = strtol (optarg, &end, 0);

	    if (end && g_strcasecmp (end, "M") == 0)
	      l <<= 20;
	    else if (end && g_strcasecmp (end, "K") == 0)
	      l <<= 10;
	    else if (end || l < 0)
	      {
		xd_error ("illegal maxmem argument %s\n", optarg);
		return 2;
	      }

	    l = MAX (l, XD_PAGE_SIZE * 8);

	    max_mapped_pages = l / XD_PAGE_SIZE;
	  }
	  break;
	case 'h': help (); break;
	case 'v': version (); break;
	case '0': case '1': case '2': case '3': case '4':
	case '5': case '6': case '7': case '8': case '9':
	  compress_level = c - '0';
	  break;
	case '?':
	default:
	  xd_error ("illegal argument, use --help for help\n");
	  return 2;
	}
    }

  if (verbose && max_mapped_pages < G_MAXINT)
    xd_error ("using %d kilobytes of buffer space\n", (max_mapped_pages * XD_PAGE_SIZE) >> 10);

  argc -= optind;
  argv += optind;

  if (cmd->nargs >= 0 && argc != cmd->nargs)
    {
      xd_error ("wrong number of arguments\n");
      help ();
      return 2;
    }

  return (* cmd->func) (argc, argv);
}

/* Commands */

static void
htonl_array (guint32* array, gint len)
{
  gint i;

  for (i = 0; i < len; i += 1)
    array[i] = g_htonl(array[i]);
}

static void
ntohl_array (guint32* array, gint len)
{
  gint i;

  for (i = 0; i < len; i += 1)
    array[i] = g_ntohl(array[i]);
}

static gint
delta_command (gint argc, gchar** argv)
{
  gint patch_out_fd;
  const char* patch_out_name;
  FileHandle *from, *to, *out, *data_out, *cont_out;
  XdeltaGenerator* gen;
  XdeltaSource* src;
  XdeltaControl* cont;
  gboolean from_is_compressed = FALSE, to_is_compressed = FALSE;
  guint32 control_offset, header_offset;
  const char* from_name, *to_name;
  guint32 header_space[HEADER_WORDS];

  memset (header_space, 0, sizeof (header_space));

  /* This could read tofile from stdin... */
  if (argc < 2 || argc > 3)
    {
      xd_error ("usage: %s delta fromfile tofile [patchfile]\n", program_name);
      return 2;
    }

  if (! (from = open_read_seek_handle (argv[0], &from_is_compressed, TRUE)))
    return 2;

  if (! (to = open_read_noseek_handle (argv[1], &to_is_compressed)))
    return 2;

  if (argc == 2 || strcmp (argv[2], "-") == 0)
    {
      patch_out_fd = STDOUT_FILENO;
      patch_out_name = "standard output";
    }
  else
    {
      int fd = open (argv[2], O_WRONLY | O_CREAT | O_TRUNC | O_BINARY, 0666);

      if (fd < 0)
	{
	  xd_error ("open %s failed: %s\n", argv[2], g_strerror (errno));
	  return 2;
	}

      patch_out_fd = fd;
      patch_out_name = argv[2];
    }

  from_name = g_basename (argv[0]);
  to_name = g_basename (argv[1]);

  if (! (out = open_write_handle (patch_out_fd, patch_out_name, FALSE)))
    return 2;

  if (! (gen = xdp_generator_new (NULL)))
    return 2;

  if (! (src = xdp_source_new (from_name, from, NULL)))
    return 2;

  xdp_source_add (gen, src);

  if (! handle_write (out, XDELTA_PREFIX, XDELTA_PREFIX_LEN))
    return 2;

  /* compute the header */
  header_space[0] = 0;

  if (no_verify) header_space[0]           |= FLAG_NO_VERIFY;
  if (from_is_compressed) header_space[0]  |= FLAG_FROM_COMPRESSED;
  if (to_is_compressed) header_space[0]    |= FLAG_TO_COMPRESSED;
  if (compress_level != 0) header_space[0] |= FLAG_PATCH_COMPRESSED;

  header_space[1] = strlen (from_name) << 16 | strlen (to_name);
  /* end compute the header */

  htonl_array (header_space, HEADER_WORDS);

  if (! handle_write (out, (guint8*) header_space, HEADER_SPACE))
    return 2;

  if (! handle_write (out, from_name, strlen (from_name)))
    return 2;

  if (! handle_write (out, to_name, strlen (to_name)))
    return 2;

  if (! handle_close (out))
    return 2;

  if ((header_offset = handle_length (out)) < 0)
    return 2;

  if (! (data_out = compress_subhandle (out, compress_level)))
    return 2;

  if (! (cont = xdp_generate_delta (gen, to, NULL, data_out)))
    return 2;

#if 0
  serializeio_print_xdeltacontrol_obj (cont, 0);
#endif

  if (cont->has_data && cont->has_data == cont->source_info_len)
    {
      if (! quiet)
	xd_error ("warning: no matches found in from file, patch will apply without it\n");
    }

  if ((control_offset = handle_length (out)) < 0)
    return 2;

  if (! (cont_out = compress_subhandle (out, compress_level)))
    return 2;

  if (! xdp_control_write (cont, cont_out))
    return 2;

  if (! handle_putui (out, control_offset))
    return 2;

  if (! handle_write (out, XDELTA_PREFIX, XDELTA_PREFIX_LEN))
    return 2;

  handle_close (from);
  handle_close (to);

  if (! handle_close (out))
    return 2;

  return control_offset != header_offset;
}

static XdeltaPatch*
process_patch (const char* name)
{
  XdeltaPatch* patch;
  guint total_trailer;
  FileHandle *patch_control;
  guint control_end;

  patch = g_new0 (XdeltaPatch, 1);

  patch->patch_name = name;

  if (! (patch->patch_in = open_read_seek_handle (name, &patch->patch_is_compressed, FALSE)))
    return NULL;

  if (handle_read (patch->patch_in, patch->magic_buf, XDELTA_PREFIX_LEN) != XDELTA_PREFIX_LEN)
    return NULL;

  if (handle_read (patch->patch_in, (guint8*) patch->header_space, HEADER_SPACE) != HEADER_SPACE)
    return NULL;

  ntohl_array (patch->header_space, HEADER_WORDS);

  if (strncmp (patch->magic_buf, XDELTA_110_PREFIX, XDELTA_PREFIX_LEN) == 0)
    {
      patch->has_trailer = TRUE;
      patch->patch_version = "1.1";
    }
  else if (strncmp (patch->magic_buf, XDELTA_104_PREFIX, XDELTA_PREFIX_LEN) == 0)
    {
      patch->has_trailer = TRUE;
      patch->patch_version = "1.0.4";
    }
  else if (strncmp (patch->magic_buf, XDELTA_100_PREFIX, XDELTA_PREFIX_LEN))
    {
      patch->patch_version = "1.0";
    }
  else if (strncmp (patch->magic_buf, XDELTA_020_PREFIX, XDELTA_PREFIX_LEN))
    goto nosupport;
  else if (strncmp (patch->magic_buf, XDELTA_018_PREFIX, XDELTA_PREFIX_LEN))
    goto nosupport;
  else if (strncmp (patch->magic_buf, XDELTA_014_PREFIX, XDELTA_PREFIX_LEN))
    goto nosupport;
  else
    {
      xd_error ("%s: bad magic number: not a valid delta\n", name);
      return NULL;
    }

  patch->patch_flags = patch->header_space[0];

  if (no_verify)
    xd_error ("--noverify is only accepted when creating a delta\n");

  if (patch->patch_flags & FLAG_NO_VERIFY)
    no_verify = TRUE;
  else
    no_verify = FALSE;

  patch->from_name_len = patch->header_space[1] >> 16;
  patch->to_name_len = patch->header_space[1] & 0xffff;

  patch->from_name = g_malloc (patch->from_name_len+1);
  patch->to_name = g_malloc (patch->to_name_len+1);

  patch->from_name[patch->from_name_len] = 0;
  patch->to_name[patch->to_name_len] = 0;

  if (handle_read (patch->patch_in, patch->from_name, patch->from_name_len) != patch->from_name_len)
    return NULL;

  if (handle_read (patch->patch_in, patch->to_name, patch->to_name_len) != patch->to_name_len)
    return NULL;

  patch->header_offset = handle_position (patch->patch_in);

  total_trailer = 4 + (patch->has_trailer ? XDELTA_PREFIX_LEN : 0);

  control_end = handle_length (patch->patch_in) - total_trailer;

  if (! handle_seek (patch->patch_in, control_end, HANDLE_SEEK_SET))
    return NULL;

  if (! handle_getui (patch->patch_in, &patch->control_offset))
    return NULL;

  if (patch->has_trailer)
    {
      guint8 trailer_buf[XDELTA_PREFIX_LEN];

      if (handle_read (patch->patch_in, trailer_buf, XDELTA_PREFIX_LEN) != XDELTA_PREFIX_LEN)
	return NULL;

      if (strncmp (trailer_buf, patch->magic_buf, XDELTA_PREFIX_LEN) != 0)
	{
	  xd_error ("%s: bad trailing magic number, delta is corrupt\n", name);
	  return NULL;
	}
    }

  if (! (patch_control = uncompress_subhandle (patch->patch_in,
					       patch->control_offset,
					       control_end,
					       patch->patch_flags & FLAG_PATCH_COMPRESSED)))
    return NULL;

  if (! (patch->cont = xdp_control_read (patch_control)))
    return NULL;

  if (patch->cont->source_info_len > 0)
    {
      XdeltaSourceInfo* info = patch->cont->source_info[0];

      if (info->isdata)
	patch->data_source = info;
      else
	{
	  patch->from_source = info;

	  if (patch->cont->source_info_len > 1)
	    {
	      xd_generate_void_event (EC_XdIncompatibleDelta);
	      return NULL;
	    }
	}
    }

  if (patch->cont->source_info_len > 1)
    {
      patch->from_source = patch->cont->source_info[1];
    }

  if (patch->cont->source_info_len > 2)
    {
      xd_generate_void_event (EC_XdIncompatibleDelta);
      return NULL;
    }

  if (! (patch->patch_data = uncompress_subhandle (patch->patch_in,
						   patch->header_offset,
						   patch->control_offset,
						   patch->patch_flags & FLAG_PATCH_COMPRESSED)))
    return NULL;

  return patch;

 nosupport:

  xd_error ("delta format is unsupported (too old)\n");
  return NULL;
}

static gint
info_command (gint argc, gchar** argv)
{
  XdeltaPatch* patch;
  char buf[33];
  int i;
  XdeltaSourceInfo* si;

  if (! (patch = process_patch (argv[0])))
    return 2;

  xd_error_handle = _stdout_handle;

  xd_error ("version %s found patch version %s in %s%s\n",
	    xdelta_version,
	    patch->patch_version,
	    patch->patch_name,
	    patch->patch_flags & FLAG_PATCH_COMPRESSED ? " (compressed)" : "");

  if (patch->patch_flags & FLAG_NO_VERIFY)
    xd_error ("generated with --noverify\n");

  if (patch->patch_flags & FLAG_FROM_COMPRESSED)
    xd_error ("generated with a gzipped FROM file\n");

  if (patch->patch_flags & FLAG_TO_COMPRESSED)
    xd_error ("generated with a gzipped TO file\n");

  edsio_md5_to_string (patch->cont->to_md5, buf);

  xd_error ("output name:   %s\n", patch->to_name);
  xd_error ("output length: %d\n", patch->cont->to_len);
  xd_error ("output md5:    %s\n", buf);

  xd_error ("patch from segments: %d\n", patch->cont->source_info_len);

  xd_error ("MD5\t\t\t\t Length\tCopies\tUsed\tSeq?\tName\n");

  for (i = 0; i < patch->cont->source_info_len; i += 1)
    {
      si = patch->cont->source_info[i];

      edsio_md5_to_string (si->md5, buf);

      xd_error ("%s %d\t%d\t%d\t%s\t%s\n",
		buf,
		si->len,
		si->copies,
		si->copy_length,
		si->sequential ? "yes" : "no",
		si->name);
    }

  return 0;
}

static gint
patch_command (gint argc, gchar** argv)
{
  FileHandle* to_out;
  XdeltaPatch* patch;
  gint to_out_fd;

  if (argc < 1 || argc > 3)
    {
      xd_error ("usage: %s patch patchfile fromfile [tofile]\n", program_name);
      return 2;
    }

  if (! (patch = process_patch (argv[0])))
    return 2;

  if (argc > 1)
    patch->from_name = argv[1];
  else if (verbose)
    xd_error ("using default from file name: %s\n", patch->from_name);

  if (argc > 2)
    patch->to_name = argv[2];
  else if (verbose)
    xd_error ("using default to file name: %s\n", patch->to_name);

  if (strcmp (patch->to_name, "-") == 0)
    {
      to_out_fd = STDOUT_FILENO;
      patch->to_name = "standard output";
    }
  else
    {
      to_out_fd = open (patch->to_name, O_WRONLY | O_CREAT | O_TRUNC | O_BINARY, 0666);

      if (to_out_fd < 0)
	{
	  xd_error ("open %s failed: %s\n", patch->to_name, g_strerror (errno));
	  return 2;
	}
    }

  to_out = open_write_handle (to_out_fd, patch->to_name, patch->patch_flags & FLAG_TO_COMPRESSED);

  if (patch->from_source)
    {
      FileHandle* from_in;
      gboolean from_is_compressed = FALSE;

      if (! (from_in = open_read_seek_handle (patch->from_name, &from_is_compressed, TRUE)))
	return 2;

      if (from_is_compressed != ((patch->patch_flags & FLAG_FROM_COMPRESSED) && 1))
	xd_error ("warning: expected %scompressed from file\n", (patch->patch_flags & FLAG_FROM_COMPRESSED) ? "" : "un");

      if (handle_length (from_in) != patch->from_source->len)
	{
	  xd_error ("expected from file of %slength %d bytes\n",
		    from_is_compressed ? "uncompressed " : "",
		    patch->from_source->len);
	  return 2;
	}

      patch->from_source->in = from_in;
    }

  if (patch->data_source)
    patch->data_source->in = patch->patch_data;

  if (! xdp_apply_delta (patch->cont, to_out))
    return 2;

  if (patch->from_source)
    handle_close (patch->from_source->in);

  handle_close (patch->patch_data);

  if (! handle_close (to_out))
    return 2;

  return 0;
}
